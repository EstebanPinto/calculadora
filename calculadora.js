class calculadora {
    //Atributos
    num1;
    num2;

    //Metodos

    constructor(num1,num2){
        this.num1=num1;
        this.num2=num2;
    }
    

    sumar(){
        let suma;
        suma=this.num1+this.num2;
        console.log("La suma es: ", suma);
    }

    restar(){
        let resta;
        resta=this.num1-this.num2;
        console.log("La resta es: ", resta);
    }

    multiplicar(){
        let multi;
        multi=this.num1*this.num2;
        console.log("La multiplicacion es: ", multi);
    }

    dividir(){
        let divi;
        divi=this.num1/this.num2;
        console.log("La divicion es: ", divi);
    }

    modulo(){
        let mod;
        mod=this.num1%this.num2;
        console.log("El modulo es: ", mod);
    }
}

//Herencias

class CalculadoraCientifica extends calculadora{
    
    calcularseno(){
        let result1, result2;
        result1 = Math.sin(this.num1);
        result2 = Math.sin(this.num2);

        console.log("El seno del primer numero es: ", result1);
        console.log("El seno del segundo numero es: ", result2);
    }

    calcularcoseno(){
        let result1, result2;
        result1 = Math.cos(this.num1);
        result2 = Math.cos(this.num2);

        console.log("El coseno del primer numero es: ", result1);
        console.log("El coseno del segundo numero es: ", result2);
    }

    calculartangente(){
        let result1, result2;
        result1 = Math.tan(this.num1);
        result2 = Math.tan(this.num2);

        console.log("El tangente del primer numero es: ", result1);
        console.log("El tangente del segundo numero es: ", result2);
    }
}

class CalculadoraConversora extends CalculadoraCientifica{

    gradarad(){
        let rad1, rad2;

        rad1=(this.num1)*(Math.PI/180);
        rad2=(this.num2)*(Math.PI/180);

        console.log("El numero en radianes es: ", rad1);
        console.log("El numero en radianes es: ", rad2);
    }

    radagrad(){
        let grad1, grad2;

        grad1=(this.num1)/(Math.PI/180);
        grad2=(this.num2)/(Math.PI/180);

        console.log("El numero en radianes es: ", grad1);
        console.log("El numero en radianes es: ", grad2);
    }

    kelacen(){
        let cen1, cen2;

        cen1 = this.num1 - 273.15;
        cen2 = this.num2 - 273.15;

        console.log("El numero en centigrados es: ", cen1);
        console.log("El numero en centigrados es: ", cen2);
    }

    cenakel(){
        let kel1, kel2;

        kel1 = this.num1 + 273.15;
        kel2 = this.num2 + 273.15;

        console.log("El numero en kelvin es: ", kel1);
        console.log("El numero en kelvin es: ", kel2);
    }
}

